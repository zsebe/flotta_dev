/**
 * View Models used by Spring MVC REST controllers.
 */
package mark.flotta.app.web.rest.vm;
